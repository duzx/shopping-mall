package com.duzx.mall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.duzx.common.utils.PageUtils;
import com.duzx.mall.product.entity.SpuInfoEntity;

import java.util.Map;

/**
 * spu信息
 *
 * @author duzx
 * @email duzx5517@163.com
 * @date 2021-03-23 23:18:54
 */
public interface SpuInfoService extends IService<SpuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

