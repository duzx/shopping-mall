package com.duzx.mall.product.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.duzx.common.utils.PageUtils;
import com.duzx.common.utils.Query;

import com.duzx.mall.product.dao.CategoryDao;
import com.duzx.mall.product.entity.CategoryEntity;
import com.duzx.mall.product.service.CategoryService;


@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

    @Autowired
    private CategoryDao categoryDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> listTree(Map<String, Object> params) {
        List<CategoryEntity> categoryList = categoryDao.selectList(null);

        List<CategoryEntity> level1 = categoryList.stream().filter(item ->
            item.getParentCid() == 0
        ).map(menu -> {
            menu.setChildren(getChildrens(menu, categoryList));
            return menu;
        }).sorted(Comparator.comparingInt(CategoryEntity::getSort))
                .collect(Collectors.toList());

        return categoryList;
    }

    private List<CategoryEntity> getChildrens(CategoryEntity root, List<CategoryEntity> list){
        List<CategoryEntity> children = list.stream().filter(categoryEntity -> {
            return categoryEntity.getParentCid() == root.getCatId();
        }).map(item ->{
            item.setChildren(getChildrens(item, list));
            return item ;
        }).collect(Collectors.toList());

        return children;
    }

}