package com.duzx.mall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;

import com.duzx.common.utils.PageUtils;
import com.duzx.mall.product.entity.BrandEntity;

import java.util.Map;

/**
 * 品牌
 *
 * @author duzx
 * @email duzx5517@163.com
 * @date 2021-03-23 23:18:54
 */
public interface BrandService extends IService<BrandEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

