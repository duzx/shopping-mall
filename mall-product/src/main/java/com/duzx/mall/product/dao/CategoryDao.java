package com.duzx.mall.product.dao;

import com.duzx.mall.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

/**
 * 商品三级分类
 * 
 * @author duzx
 * @email duzx5517@163.com
 * @date 2021-03-23 23:18:54
 */
@Mapper
@Component
@Repository
public interface CategoryDao extends BaseMapper<CategoryEntity> {
	
}
