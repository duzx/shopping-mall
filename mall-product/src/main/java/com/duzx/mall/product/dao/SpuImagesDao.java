package com.duzx.mall.product.dao;

import com.duzx.mall.product.entity.SpuImagesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu图片
 * 
 * @author duzx
 * @email duzx5517@163.com
 * @date 2021-03-23 23:18:54
 */
@Mapper
public interface SpuImagesDao extends BaseMapper<SpuImagesEntity> {
	
}
