package com.duzx.mall.product;

import com.duzx.mall.product.entity.BrandEntity;
import com.duzx.mall.product.service.BrandService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MallProductApplicationTests {

    @Autowired
    BrandService brandService;

    @Test
    void contextLoads() {
        BrandEntity brandEntity = new BrandEntity();
        brandEntity.setDescript("123123");

        brandService.save(brandEntity);
        System.err.println("保存成功");
    }

}
