package com.duzx.mall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.duzx.common.utils.PageUtils;
import com.duzx.mall.member.entity.MemberCollectSpuEntity;

import java.util.Map;

/**
 * 会员收藏的商品
 *
 * @author duzx
 * @email duzx5517@163.com
 * @date 2021-03-24 20:42:49
 */
public interface MemberCollectSpuService extends IService<MemberCollectSpuEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

