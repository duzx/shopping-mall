package com.duzx.mall.member.dao;

import com.duzx.mall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author duzx
 * @email duzx5517@163.com
 * @date 2021-03-24 20:42:49
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
