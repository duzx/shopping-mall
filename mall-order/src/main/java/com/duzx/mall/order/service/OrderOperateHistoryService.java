package com.duzx.mall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.duzx.common.utils.PageUtils;
import com.duzx.mall.order.entity.OrderOperateHistoryEntity;

import java.util.Map;

/**
 * 订单操作历史记录
 *
 * @author duzx
 * @email duzx5517@163.com
 * @date 2021-03-24 20:51:56
 */
public interface OrderOperateHistoryService extends IService<OrderOperateHistoryEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

