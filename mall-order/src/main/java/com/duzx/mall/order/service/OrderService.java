package com.duzx.mall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.duzx.common.utils.PageUtils;
import com.duzx.mall.order.entity.OrderEntity;

import java.util.Map;

/**
 * 订单
 *
 * @author duzx
 * @email duzx5517@163.com
 * @date 2021-03-24 20:51:55
 */
public interface OrderService extends IService<OrderEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

