package com.duzx.mall.order.dao;

import com.duzx.mall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author duzx
 * @email duzx5517@163.com
 * @date 2021-03-24 20:51:55
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
