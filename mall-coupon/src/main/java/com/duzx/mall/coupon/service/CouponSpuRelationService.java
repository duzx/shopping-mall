package com.duzx.mall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.duzx.common.utils.PageUtils;
import com.duzx.mall.coupon.entity.CouponSpuRelationEntity;

import java.util.Map;

/**
 * 优惠券与产品关联
 *
 * @author duzx
 * @email duzx5517@163.com
 * @date 2021-03-24 00:22:54
 */
public interface CouponSpuRelationService extends IService<CouponSpuRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

