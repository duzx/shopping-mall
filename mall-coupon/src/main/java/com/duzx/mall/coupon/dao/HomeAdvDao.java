package com.duzx.mall.coupon.dao;

import com.duzx.mall.coupon.entity.HomeAdvEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 首页轮播广告
 * 
 * @author duzx
 * @email duzx5517@163.com
 * @date 2021-03-24 00:22:54
 */
@Mapper
public interface HomeAdvDao extends BaseMapper<HomeAdvEntity> {
	
}
