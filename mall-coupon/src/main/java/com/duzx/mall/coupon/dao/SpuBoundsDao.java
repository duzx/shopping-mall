package com.duzx.mall.coupon.dao;

import com.duzx.mall.coupon.entity.SpuBoundsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品spu积分设置
 * 
 * @author duzx
 * @email duzx5517@163.com
 * @date 2021-03-24 00:22:54
 */
@Mapper
public interface SpuBoundsDao extends BaseMapper<SpuBoundsEntity> {
	
}
