package com.duzx.mall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.duzx.common.utils.PageUtils;
import com.duzx.mall.coupon.entity.HomeSubjectSpuEntity;

import java.util.Map;

/**
 * 专题商品
 *
 * @author duzx
 * @email duzx5517@163.com
 * @date 2021-03-24 00:22:54
 */
public interface HomeSubjectSpuService extends IService<HomeSubjectSpuEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

